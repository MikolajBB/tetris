import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.util.Random;

/**
 * Created by Mikołaj on 10.05.2017.
 */


public class Board extends JPanel implements ActionListener {

    private static final int BOARD_COLUMNS = 10;
    private static final int BOARD_ROWS = 20;
    private static final int TILE_SIZE = 24;

    private String filePath = "Scores.bin";
    private Userbase scoreTable = new Userbase(filePath);

    private  int points = 1;
    private  int lvl = 1;
    private  int delay = 400;

    private Tetris tetris;
    private Shape shape;
    private int rotation;
    private Random r = new Random();
    private Timer timer;
    private int curX, curY;
    private int[][] boardTable = new int[BOARD_ROWS + 4][BOARD_COLUMNS + 2];


    public Board(Tetris tetris){

        this.tetris = tetris;
        setFocusable(true);
        setPreferredSize(new Dimension(250,490));
        setBackground(new Color(1,1,1));

        timer = new Timer(delay, this);
        timer.start();

        shape = Shape.randShape();
        rotation = r.nextInt(3);

        this.curY = -72;
        this.curX = 48;
        setBoardTable();

        addKeyListener(new TAdapter());
        setVisible(true);
    }

    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);

        drawGrid(g, BOARD_COLUMNS, BOARD_ROWS);

        for(int i = 3; i < BOARD_ROWS + 3 ; i++){
            for(int j = 1; j < BOARD_COLUMNS + 1 ; j++){
                if(this.boardTable[i][j] > 0 && this.boardTable[i][j] < 8)
                    drawSquare(g, ((j-1) * TILE_SIZE), ((i-3) * TILE_SIZE), this.boardTable[i][j]);
            }
        }

        drawPiece(g, this.curX, this.curY, shape, rotation);
        g.setColor(new Color(144, 0, 63));

        g.drawRect(0, 0, 240, 480);

    }


    @Override
    public void actionPerformed(ActionEvent e) {

            oneLineDown();

    }

    class TAdapter extends KeyAdapter {

        private int pause = 0;

        @Override
        public void keyPressed(KeyEvent e) {


            int keycode = e.getKeyCode();

            switch (keycode) {

                case KeyEvent.VK_LEFT:
                    if(!tryMoveLeft(TILE_SIZE)) return;

                    break;
                case KeyEvent.VK_RIGHT:
                    if(!tryMoveRight(TILE_SIZE)) return;

                    break;
                case KeyEvent.VK_DOWN:
                    if(getCurY() >= 0)
                    oneLineDown();

                    break;
                case KeyEvent.VK_UP:
                    if(!tryRotatePiece()) return;

                    break;
                case KeyEvent.VK_SPACE:

                    if(pause == 0) {
                        timer.stop();
                        this.pause = 1;
                    }
                    else {
                        this.pause = 0;
                        timer.start();
                    }
                    break;
            }

        }
    }


    public void oneLineDown(){

        isGameOver();
        if(isAvailableSpaceBottom(this.curY, this.curX)) {
            repaint();
            setCurY(TILE_SIZE);

        }

        else {

            updateBoard(this.curX, this.curY, shape);
            removeFullLines();
            setLvl(this.points);
            setCurY(-72);
            setCurX(48);
            setRandomPiece();

        }
    }

    public boolean isAvailableSpaceBottom(int curY, int curX){

        curY = curY / 24;
        curX = curX / 24;

        boolean[][][] shapeCoords = shape.getShapeCoords();


        for (int i = shape.getSizeX() - 1; i >= 0; i--) {
            for (int j = shape.getSizeY() - 1; j >= 0; j--) {
                if (shapeCoords[rotation][i][j] && (isPiece((curY + 3) + i + 1, curX + j + 1)))
                    return false;
            }
        }

        return true;
    }

    public boolean isAvailableSpaceRight(int curX, int curY){

        curY = curY / 24;
        curX = curX / 24;
        boolean[][][] shapeCoords = shape.getShapeCoords();


            for (int j = shape.getSizeY() - 1; j >= 0; j--) {
                for (int i = shape.getSizeX() - 1; i >= 0; i--) {
                    if (shapeCoords[rotation][i][j] && isPiece(curY + i + 3, (curX + j) + 1))
                        return false;
                }
            }

            return true;

    }

    public boolean isAvailableSpaceLeft(int curX, int curY){

        curY = curY / 24;
        curX = curX / 24;

            boolean[][][] shapeCoords = shape.getShapeCoords();


            for (int j = 0; j < shape.getSizeY(); j++) {
                for (int i = 0; i < shape.getSizeX(); i++) {
                    if (shapeCoords[rotation][i][j] && isPiece(curY + i + 3, curX + j))
                        return false;
                }
            }

            return true;
      //  }

    }

    public boolean isAvailableSpaceForRotation(int curX, int curY, int rotation, Shape shape){

        curY = curY / 24;
        curX = curX / 24;
        if(curX < 0) return false;

        rotation++;
        if(rotation > 3) rotation = 0;

        boolean[][][] shapeCoords = shape.getShapeCoords();

        for (int i = shape.getSizeX() - 1; i >= 0; i--) {
            for (int j = shape.getSizeY() -1; j >= 0; j--) {
                if (shapeCoords[rotation][i][j] && (isPiece(curY + i + 3, curX + j + 1)))
                    return false;
            }
        }
        return true;
    }

    private boolean isPiece(int x, int y){

        return ((this.boardTable[x][y] == -1 || (this.boardTable[x][y] > 0 && this.boardTable[x][y] <= 7)));
    }



    public void updateBoard(int curX, int curY, Shape shape){

        curX = curX / 24;
        curY = curY / 24;

        boolean[][][] shapeCoords = shape.getShapeCoords();

        for (int i = 0; i < shape.getSizeX(); i++) {
            for (int j = 0; j < shape.getSizeY(); j++) {
                if(shapeCoords[rotation][i][j])
                    this.boardTable[(curY+3 + i)][(curX + j) + 1] = shape.ordinal() + 1;
            }
        }

        for(int i = 0; i < BOARD_ROWS + 4; i++){
            System.out.print("\n");
            for(int j = 0; j < BOARD_COLUMNS + 2 ; j++){
                System.out.print(this.boardTable[i][j] + " ");
            }
        }
        System.out.println("\n");
    }

    public void removeFullLines() {

        int counter;
        int c = 1;
        for (int i = 0; i < BOARD_ROWS+3; i++) {
            counter = 0;
            for (int j = 1; j < BOARD_COLUMNS + 1; j++) {

                if (this.boardTable[i][j] > 0 && this.boardTable[i][j] < 8) counter++;

                if (counter == 10) {

                    this.points += (c * 10);
                    c+=3;

                    for (int k = i; k > 0; k--) {
                        for (j = 1; j < BOARD_COLUMNS + 1; j++) {

                            this.boardTable[k][j] = this.boardTable[k-1][j];
                        }
                    }
                    counter = 0;
                }
            }
        }
    }


    public void drawGrid(Graphics g, int rows, int columns){

        g.setColor(new Color(78, 82, 83));

        for(int i = 0; i < rows * TILE_SIZE; i+=TILE_SIZE){
            for(int j = 0; j < columns * TILE_SIZE; j+=TILE_SIZE){

                g.drawRect(i, j, TILE_SIZE, TILE_SIZE);
            }
        }
    }

    public void drawSquare(Graphics g, int x, int y, int shapeOrdinal){

        Color color = shape.getColorByOrdinal(shapeOrdinal);
        g.setColor(color);
        g.fillRect(x , y ,TILE_SIZE , TILE_SIZE );
        g.setColor(color.darker());
        g.fillRect(x, y + TILE_SIZE - 4, TILE_SIZE, 4);
        g.fillRect(x + TILE_SIZE - 4, y, 4, TILE_SIZE);
        g.setColor(color.brighter());
        for(int i = 0; i < 4; i++) {
            g.drawLine(x, y + i, x + TILE_SIZE - i - 1, y + i);
            g.drawLine(x + i, y, x + i, y + TILE_SIZE - i - 1);
        }

    }

    public void drawPiece(Graphics g,int x, int y, Shape shape, int rotation){

        boolean[][][] coords = shape.getShapeCoords();
        int coordX = x, coordY = y;

        int sizeX = shape.getSizeX();
        int sizeY = shape.getSizeY();

        for(int i = 0; i < sizeX; i++){
            y = (TILE_SIZE * i);

            for(int j = 0; j < sizeY; j++){
                x = (TILE_SIZE * j);

                if(coords[rotation][i][j]){
                    drawSquare(g, x + coordX, y + coordY, shape.ordinal() + 1);
                }
            }
        }
    }


    public int getCurY() {
        return curY;
    }

    public void setCurX(int curX) {
        this.curX = curX;
    }

    public boolean tryRotatePiece(){

        if(this.curY < 0) return false;

        if(isAvailableSpaceForRotation(this.curX, this.curY, this.rotation, this.shape)) {

            this.rotation++;
            if(this.rotation > 3) this.rotation = 0;
            repaint();
            return true;
        }

        else {
            return false;
        }
    }

    public boolean tryMoveLeft(int movement){

            if(this.curY < 0) return false;

            if (isAvailableSpaceLeft(this.curX, this.curY)) {
                this.curX -= movement;
                repaint();
                return true;

            } else {
                return false;
            }
        }

    public boolean tryMoveRight(int movement){

        if(this.curY < 0) return false;

        this.curX += movement;

        if(isAvailableSpaceRight(this.curX, this.curY)) {
            repaint();
            return true;
        }
        else {
            this.curX -= movement;
            return false;
        }
    }


    public int getPoints() {
        return points;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int points) {


        if(points > (50*lvl)) {

            this.lvl++;
            if(delay > 50) delay-=20;
            timer.setDelay(delay);
        }
    }

    public void setCurY(int curY) {
        if(curY <= 0) this.curY = curY;
        else this.curY += curY;
    }

    public void setBoardTable() {


        for(int i = 1; i < BOARD_ROWS + 4; i++){
            for(int j = 0; j < BOARD_COLUMNS + 2 ; j++){
                this.boardTable[i][j] = -1;
            }
        }

        for(int i = 0; i < BOARD_ROWS + 3 ; i++){
            for(int j = 1; j < BOARD_COLUMNS + 1 ; j++){
                this.boardTable[i][j] = 0;
            }
        }

        for(int i = 0; i < BOARD_ROWS + 4; i++){
            System.out.print("\n");
            for(int j = 0; j < BOARD_COLUMNS + 2 ; j++){
                System.out.print(this.boardTable[i][j] + " ");
            }
        }

    }

    public void setRandomPiece(){
        this.shape = Shape.randShape();
        this.rotation = r.nextInt(3);
    }

    private  void isGameOver(){

            for(int j = 1; j < BOARD_COLUMNS + 1; j++)
                if(isPiece(3, j)) {
                    this.timer.stop();
                    closeGame();
                    break;
                }

        }

    public  void closeGame(){

        this.setFocusable(false);
        tetris.setVisible(false);

        User user = new User(LoginPanel.getNameUser(), this.points);
        scoreTable.addUserToList(user);
        scoreTable.writeToFile(filePath);
        new EndPanel(this).setVisible(true);
    }
}


