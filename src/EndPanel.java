import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Mikołaj on 18.06.2017.
 */
public class EndPanel extends JFrame implements ActionListener {

    private JList tableScore;
    private JButton exitGame;
    private JButton playAgain;
    private Board board;
    private Userbase base = new Userbase("Scores.bin");

    public EndPanel(Board board){

        super("End");
        setSize(new Dimension(400,400));
        setLayout(null);

        this.board = board;

        tableScore = new JList(base.getUserList().toArray());
        tableScore.setFont(new Font("Tahoma", Font.BOLD, 15));
        tableScore.setForeground(new Color(8, 164, 17));
        tableScore.setBackground(new Color(1,1,1));
        tableScore.setBorder(null);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(tableScore);
        scrollPane.setBounds(45,120,300,150);
        scrollPane.setBorder(null);

        exitGame = new JButton("Wyjdz z gry");
        exitGame.setBounds(50,290,100,30);
        exitGame.addActionListener(this);

        playAgain = new JButton("Zagraj ponownie");
        playAgain.setBounds(200,290,150,30);
        playAgain.addActionListener(this);


        add(scrollPane);
        add(playAgain);
        add(exitGame);

        getContentPane().setBackground(new Color(5, 5, 5));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }


    @Override
    public void paint(Graphics g) {
        super.paint(g);

        g.setColor(new Color(8, 102, 16));
        g.setFont(new Font("Tahoma", Font.BOLD, 20));
        g.drawString("Osiagnales:   " + board.getPoints() + " punktow" , 50, 100);

        g.setColor(new Color(8, 102, 16));
        g.setFont(new Font("Tahoma", Font.BOLD, 15));
        g.drawString("Tabela wynikow: ",50,140);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == exitGame) System.exit(1);
        else if(e.getSource() == playAgain) {
            new Tetris().setVisible(true);
        }
    }
}
