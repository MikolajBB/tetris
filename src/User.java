import java.io.Serializable;

/**
 * Created by Mikołaj on 18.06.2017.
 */

public class User implements Comparable, Serializable {

    private String nick;
    private int score;

    public User(String nick, int score){

        this.nick = nick;
        this.score = score;
    }


    @Override
    public int compareTo(Object compareUser) {

        int compareScore=((User)compareUser).getScore();
        return compareScore  - this.score;
    }


    public String getNick() {
        return nick;
    }

    public int getScore() {
        return score;
    }

    @Override
    public String toString() {
        return  nick+":  "+score;
    }
}
