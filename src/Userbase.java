import java.io.*;
import java.util.ArrayList;
import java.util.Collections;


/**
 * Created by Mikołaj on 18.06.2017.
 */
public class Userbase {


    private ArrayList<User> userList = new ArrayList<User>();


    public Userbase(String filePath){

        readFromFile(filePath);
    }

    public  void readFromFile(String filePath){

        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(filePath))) {

            this.userList = (ArrayList) inputStream.readObject();

        }catch(ClassNotFoundException | IOException e){

            System.out.println(e.getMessage());
        }
    }

    public void writeToFile(String filePath){

        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(filePath))) {

            outputStream.writeObject(userList);

        }catch(IOException e){

            System.out.println(e.getMessage());
        }

    }

    public void addUserToList(User user){

        if(!isUserExist(user)){
            userList.add(user);
            Collections.sort(userList);
        }

        for(User str: userList){
            System.out.println(str);
        }

    }

    private boolean isUserExist(User user){

        for (int i = 0; i < userList.size(); i++) {

            if( (userList.get(i).getNick()).equals(user.getNick()) )

                if( userList.get(i).getScore() <= user.getScore() ){

                    userList.set(i , user);
                    return true;

                }

        }
        return false;
    }

    public ArrayList<User> getUserList() {
        return userList;
    }


}
