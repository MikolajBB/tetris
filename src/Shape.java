import java.awt.*;
import java.util.Random;

/**
 * Created by Mikołaj on 09.05.2017.
 */


public enum Shape {


    TypeI( new Color(70, 174, 36), 4, 4, 4, new boolean[][][] {
            {
                    {false,	false,	false,	false},
                    {true,	true,	true,	true},
                    {false,	false,	false,	false},
                    {false,	false,	false,	false},
            },
            {
                    {false,	false,	true,	false},
                    {false,	false,	true,	false},
                    {false,	false,	true,	false},
                    {false,	false,	true,	false},
            },
            {
                    {false,	false,	false,	false},
                    {false,	false,	false,	false},
                    {true,	true,	true,	true},
                    {false,	false,	false,	false},
            },
            {
                    {false,	true,	false,	false},
                    {false,	true,	false,	false},
                    {false,	true,	false,	false},
                    {false,	true,	false,	false},
            }
    }),

    TypeJ( new Color(0, 170, 174), 4, 3, 3, new boolean[][][] {
            {
                    {true,	false,	false},
                    {true,	true,	true},
                    {false,	false,	false},
            },
            {
                    {false,	true,	true},
                    {false,	true,	false},
                    {false,	true,	false},
            },
            {
                    {false,	false,	false},
                    {true,	true,	true},
                    {false,	false,	true},
            },
            {
                    {false,	true,	false},
                    {false,	true,	false},
                    {true,	true,	false},
            }
    }),

    TypeL(new Color(69, 0, 174), 4, 3, 3, new boolean[][][] {
            {
                    {false,	false,	true},
                    {true,	true,	true},
                    {false,	false,	false},
            },
            {
                    {false,	true,	false},
                    {false,	true,	false},
                    {false,	true,	true},
            },
            {
                    {false,	false,	false},
                    {true,	true,	true},
                    {true,	false,	false},
            },
            {
                    {true,	true,	false},
                    {false,	true,	false},
                    {false,	true,	false},
            }
    }),

    TypeO(new Color(166, 0, 174), 4, 2, 2, new boolean[][][] {
            {
                    {true,	true},
                    {true,	true},
            },
            {
                    {true,	true},
                    {true,	true},
            },
            {
                    {true,	true},
                    {true,	true},
            },
            {
                    {true,	true},
                    {true,	true},
            }
    }),

    TypeS(new Color(0,1, 174), 4, 3, 3, new boolean[][][] {
            {
                    {false,	true,	true},
                    {true,	true,	false},
                    {false,	false,	false},
            },
            {
                    {false,	true,	false},
                    {false,	true,	true},
                    {false,	false,	true},
            },
            {
                    {false,	false,	false},
                    {false,	true,	true},
                    {true,	true,	false},
            },
            {
                    {true,	false,	false},
                    {true,	true,	false},
                    {false,	true,	false},
            }
    }),


    TypeT(new Color(214, 193, 0), 4, 3, 3, new boolean[][][] {
            {
                    {false,	true,	false},
                    {true,	true,	true},
                    {false,	false,	false},
            },
            {
                    {false,	true,	false},
                    {false,	true,	true},
                    {false,	true,	false},
            },
            {
                    {false,	false,	false},
                    {true,	true,	true},
                    {false,	true,	false},
            },
            {
                    {false,	true,	false},
                    {true,	true,	false},
                    {false,	true,	false},
            }
    }),

    TypeZ(new Color(174, 0, 3), 4, 3, 3, new boolean[][][] {
            {
                    {true,	true,	false},
                    {false,	true,	true},
                    {false,	false,	false},
            },
            {
                    {false,	false,	true},
                    {false,	true,	true},
                    {false,	true,	false},
            },
            {
                    {false,	false,	false},
                    {true,	true,	false},
                    {false,	true,	true},
            },
            {
                    {false,	true,	false},
                    {true,	true,	false},
                    {true,	false,	false},
            }
    }),

    TypeNULL(new Color(1,1,1), 0, 4, 4, new boolean[][][]{
            {
                    {false, false, false, false},
                    {false, false, false, false},
                    {false, false, false, false},
                    {false, false, false, false},
            }
    });

    private Color shapeColor;
    private int rotation;
    private int sizeX;
    private int sizeY;
    private boolean[][][] shapeCoords;

    Shape(Color color, int rotation, int sizeX,int sizeY, boolean[][][] coords){

        this.shapeColor = color;
        this.rotation = rotation;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.shapeCoords = coords;
    }

    public static Shape randShape(){

        Random rand = new Random();
        Shape [] values = Shape.values();
        return values[rand.nextInt(6)];
    }


    public int getRotation() {
        return rotation;
    }

    public Color getColorByOrdinal(int ordinal){

        ordinal--;
        Shape [] values = Shape.values();
        return values[ordinal].getShapeColor();
    }

    public Color getShapeColor() {
        return shapeColor;
    }

    public int getSizeX() {
        return sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    public boolean[][][] getShapeCoords() {
        return shapeCoords;
    }

    public void setRotation(int rotation) {
        this.rotation = rotation;
    }

}

