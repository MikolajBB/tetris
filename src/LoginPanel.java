/**
 * Created by Mikołaj on 10.05.2017.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class LoginPanel extends JFrame implements ActionListener {

    private JButton sign;
    private JTextField insertName;
    private static String  nameUser;

    public LoginPanel(){

        super("Tetris");
        setSize(new Dimension(400,400));
        setLayout(null);


        insertName = new JTextField();
        insertName.setBounds(125,200,130,30);
        insertName.setBackground(new Color(249, 249, 249));
        insertName.setForeground(new Color(5, 5, 5));
        insertName.setFont(new Font("Tahoma", Font.BOLD, 15));
        insertName.setBorder(null);

        sign = new JButton("OK!");
        sign.setBounds(140,250,100,30);
        sign.addActionListener(this);

        add(sign);
        add(insertName);

        getContentPane().setBackground(new Color(5, 5, 5));
        setVisible(true);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if( (insertName.getText().length() >= 1) && (insertName.getText().length() <= 7) && ((insertName.getText().matches("[a-zA-Z0-9]*"))) ) {
            nameUser = insertName.getText();
            setVisible(false);
            new Tetris().setVisible(true);
        }


    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        g.setColor(new Color(8, 102, 16));
        g.setFont(new Font("Comic Sans MS", Font.BOLD, 50));
        g.drawString("T E T R I S", 50, 150);

        g.setColor(new Color(8, 102, 16));
        g.setFont(new Font("Tahoma", Font.BOLD, 15));
        g.drawString("Podaj nick:(Max 7 znakow)",136,220);
    }

    public static String getNameUser() {
        return nameUser;
    }



}
