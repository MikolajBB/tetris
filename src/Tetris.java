
import javax.swing.*;
import java.awt.*;

/**
 * Created by Mikołaj on 10.05.2017.
 */
public class Tetris extends JFrame {

    private Board board;
    private SidePanel side;

    public Tetris(){

        super("Tetris");
        setLayout(new BorderLayout());
        setResizable(false);


        this.board = new Board(this);
        this.side = new SidePanel(this.board);


        add(board, BorderLayout.CENTER);
        add(side, BorderLayout.EAST);

        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
