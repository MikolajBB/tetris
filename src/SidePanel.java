
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Mikołaj on 10.05.2017.
 */



public class SidePanel extends JPanel implements ActionListener {

    private Board board;
    private Timer timer;

    public SidePanel(Board board){

        this.board = board;
        this.timer = new Timer(100,this);
        timer.start();
        setPreferredSize(new Dimension(200, 490));

        setBackground(new Color(1, 1, 1));
        setVisible(true);
    }


    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);


        drawName(g);
        drawStats(g);
        drawControls(g);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        repaint();
    }




    public void drawControls(Graphics g){

        g.setColor(new Color(8, 78, 11));

        g.setFont(new Font("Tahoma", Font.BOLD, 14));
        g.drawString("Sterowanie",20,250);

        g.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
        g.drawString("Strzałka: Lewo - Ruch w lewo", 20, 300);
        g.drawString("Strzałka: Prawo - Ruch w prawo", 20, 330);
        g.drawString("Strzałka: Dol - Przyspiesz ", 20, 360);
        g.drawString("Strzałka: Gora - Rotuj", 20, 390);
        g.drawString("Spacja - pauza", 20, 420);

        //g.drawString("P - Pause Game", 40, 310);
    }

    public void drawStats(Graphics g){

        g.setColor(new Color(8, 78, 11));
        g.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
        g.drawString("Staty", 20, 120);

        g.setFont(new Font("Tahoma", Font.BOLD, 14));
        g.drawString("Level: " + board.getLvl(), 30,150);
        g.drawString("Score: " + board.getPoints(), 30,180);
    }

    public void drawName(Graphics g){

        g.setColor(new Color(246, 253, 255));
        g.setFont(new Font("Tahoma", Font.BOLD, 15));
        g.drawString("Imie: " , 20, 50);

        g.setColor(new Color(50, 57, 188));
        g.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
        g.drawString(LoginPanel.getNameUser() , 70, 50);
    }


}


